package com.vilo.detail.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vilo.common.base.BaseViewModel
import com.vilo.common.utils.Event
import com.vilo.detail.R
import com.vilo.detail.domain.GetDailyDetailUseCase
import com.vilo.model.Daily
import com.vilo.repository.AppDispatchers
import com.vilo.repository.utils.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailViewModel(
    private val getDailyDetailUseCase: GetDailyDetailUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    private var argsLatitude: Float = 0.0f
    private var argsLongitude: Float = 0.0f
    private val _isLoading = MutableLiveData<Resource.Status>()
    private var dailySource: LiveData<Resource<List<Daily>>> = MutableLiveData()
    private val _daily = MediatorLiveData<Resource<List<Daily>>>()
    val daily: LiveData<Resource<List<Daily>>> get() = _daily

    fun loadDataWhenActivityStarts(latitude: Float, longitude: Float) {
        argsLatitude = latitude
        argsLongitude = longitude
        getUserDetail(false, latitude, longitude)
    }

    fun reloadDataWhenUserRefreshes()
            = getUserDetail(true, argsLatitude,argsLongitude)

    private fun getUserDetail(forceRefresh: Boolean, latitude: Float,longitude: Float) = viewModelScope.launch(dispatchers.main) {
        _daily.removeSource(dailySource)
        withContext(dispatchers.io) {
            dailySource = getDailyDetailUseCase(forceRefresh, latitude, longitude)
        }
        _daily.addSource(dailySource) {
            _daily.value = it
            _isLoading.value = it.status
            if (it.status == Resource.Status.ERROR) _messageError.value =
                Event(R.string.an_error_happened)
        }
    }
}