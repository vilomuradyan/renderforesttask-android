package com.vilo.detail.binding

import android.util.Log
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.vilo.detail.adapter.DailyAdapter
import com.vilo.model.Daily
import com.vilo.repository.utils.Resource

object DailyBinding {

    @BindingAdapter("app:imageUrl")
    @JvmStatic fun loadImage(view: AppCompatImageView, url: String) {
        Glide.with(view.context).load(url).into(view)
    }

    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<Daily>>?) {
        with(recyclerView.adapter as DailyAdapter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("app:showWhenLoading")
    @JvmStatic
    fun <T>showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
        Log.d(DailyBinding::class.java.simpleName, "Resource: $resource")
        if (resource != null) view.isRefreshing = resource.status == Resource.Status.LOADING
    }
}