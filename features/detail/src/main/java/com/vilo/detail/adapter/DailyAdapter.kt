package com.vilo.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vilo.detail.R
import com.vilo.detail.views.DailyItemDiffCallback
import com.vilo.detail.views.DailyViewHolder
import com.vilo.model.Daily

class DailyAdapter : RecyclerView.Adapter<DailyViewHolder>() {

    private val daily : MutableList<Daily> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyViewHolder =
       DailyViewHolder(LayoutInflater.from(parent.context).inflate(
           R.layout.item_daily, parent, false
       ))
    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        holder.bindTo(daily = daily[position])
    }

    override fun getItemCount(): Int = daily.size

    fun updateData(items: List<Daily>) {
        val diffCallback = DailyItemDiffCallback(daily, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        daily.clear()
        daily.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }
}