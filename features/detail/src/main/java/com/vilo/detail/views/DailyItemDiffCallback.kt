package com.vilo.detail.views

import androidx.recyclerview.widget.DiffUtil
import com.vilo.model.Daily

class DailyItemDiffCallback(private val oldList: List<Daily>,
                            private val newList: List<Daily>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
            = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].clouds == newList[newItemPosition].clouds
                && oldList[oldItemPosition].dew_point == newList[newItemPosition].dew_point
                && oldList[oldItemPosition].dt == newList[newItemPosition].dt
                && oldList[oldItemPosition].feels_like == newList[newItemPosition].feels_like
                && oldList[oldItemPosition].humidity == newList[newItemPosition].humidity
                && oldList[oldItemPosition].pressure == newList[newItemPosition].pressure
                && oldList[oldItemPosition].sunrise == newList[newItemPosition].sunrise
                && oldList[oldItemPosition].sunset == newList[newItemPosition].sunset
                && oldList[oldItemPosition].pop == newList[newItemPosition].pop
                && oldList[oldItemPosition].temp == newList[newItemPosition].temp
                && oldList[oldItemPosition].uvi == newList[newItemPosition].uvi
                && oldList[oldItemPosition].weather == newList[newItemPosition].weather
                && oldList[oldItemPosition].wind_deg == newList[newItemPosition].wind_deg
                && oldList[oldItemPosition].wind_speed == newList[newItemPosition].wind_speed
    }
}