package com.vilo.detail.di

import com.vilo.detail.domain.GetDailyDetailUseCase
import com.vilo.detail.views.DetailViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureDetailModule = module {
    factory { GetDailyDetailUseCase(get()) }
    viewModel { DetailViewModel(get(), get()) }
}