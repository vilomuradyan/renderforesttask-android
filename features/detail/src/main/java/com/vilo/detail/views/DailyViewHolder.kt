package com.vilo.detail.views

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vilo.detail.databinding.ItemDailyBinding
import com.vilo.model.Daily

class DailyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
    private val binding = ItemDailyBinding.bind(itemView)

    fun bindTo(daily : Daily){
        binding.daily = daily
    }
}