package com.vilo.detail.domain

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.vilo.common.utils.Constants
import com.vilo.model.Daily
import com.vilo.repository.WeatherRepository
import com.vilo.repository.utils.Resource
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class GetDailyDetailUseCase(private val repository: WeatherRepository) {

    suspend operator fun invoke(forceRefresh: Boolean, latitude : Float,
    longitude : Float): LiveData<Resource<List<Daily>>> {
        return Transformations.map(repository.getDailyWithCache(latitude.toDouble(),
            longitude.toDouble(),forceRefresh, )) {
            it.data.let{ dailyList->
                dailyList?.forEach { daily ->
                    daily.temp.day = daily.temp.day.minus(273.15).roundToInt().toDouble()
                    if (daily.weather.isNotEmpty()){
                        daily.weather.first().icon =
                            Constants.IMAGE_URL + daily.weather.first().icon + Constants.IMAGE_END_POINT
                    }
                    daily.date = convertIntToTime(daily.dt)
                }
            }
            it
        }
    }
    @SuppressLint("SimpleDateFormat")
    fun convertIntToTime(time: Int?): String {
        var newTime: Long = time?.toLong()!!
        newTime = newTime.times(1000)
        val date = Date(newTime)
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return format.format(date)
    }
}