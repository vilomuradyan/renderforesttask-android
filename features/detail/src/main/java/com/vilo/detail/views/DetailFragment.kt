package com.vilo.detail.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.vilo.common.base.BaseFragment
import com.vilo.common.base.BaseViewModel
import com.vilo.detail.adapter.DailyAdapter
import com.vilo.detail.databinding.FragmentDetailBinding
import org.koin.android.viewmodel.ext.android.viewModel

class DetailFragment : BaseFragment() {

    private lateinit var binding : FragmentDetailBinding
    private val viewModel : DetailViewModel by viewModel()
    private val args: DetailFragmentArgs by navArgs()

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadDataWhenActivityStarts(args.lat, args.long)
        configureRecyclerView()
    }

    private fun configureRecyclerView() {
        binding.dailyRv.adapter = DailyAdapter()
    }
}