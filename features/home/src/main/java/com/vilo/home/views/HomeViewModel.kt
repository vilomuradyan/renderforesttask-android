package com.vilo.home.views

import android.annotation.SuppressLint
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vilo.common.base.BaseViewModel
import com.vilo.common.utils.Event
import com.vilo.common.utils.Preference
import com.vilo.home.R
import com.vilo.home.domain.GetCurrentWeatherUseCase
import com.vilo.model.Current
import com.vilo.repository.AppDispatchers
import com.vilo.repository.utils.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class HomeViewModel(
    private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase,
    private val dispatchers: AppDispatchers,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel() {


    private val _current = MediatorLiveData<Resource<Current>>()
    val current: LiveData<Resource<Current>> get() = _current
    private var currentsSource: LiveData<Resource<Current>> = MutableLiveData()
    private val _dt = MediatorLiveData<String>()
    val dt: LiveData<String> get() = _dt

    fun refreshData(){
        getCurrent(Preference.getLatitude(sharedPreferences).toDouble(),
            Preference.getLongitude(sharedPreferences).toDouble(), true)
    }

    init {
        if (Preference.getLatitude(sharedPreferences) != 0f
            && Preference.getLongitude(sharedPreferences) != 0f){
            getCurrent(Preference.getLatitude(sharedPreferences).toDouble(),
                Preference.getLongitude(sharedPreferences).toDouble(),true)
        }
    }

    fun saveLocation(latitude: Double, longitude: Double){
        Preference.saveLatitude(latitude.toFloat(), sharedPreferences)
        Preference.saveLongitude(longitude.toFloat(), sharedPreferences)
    }

    fun moveToList()
            = navigate(
        HomeFragmentDirections.actionHomeFragmentToDetailFragment(
            Preference.getLatitude(sharedPreferences),
            Preference.getLongitude(sharedPreferences)
        )
    )

    fun getCurrent(latitude: Double, longitude: Double,forceRefresh : Boolean)
    = viewModelScope.launch(dispatchers.main) {
        _current.removeSource(currentsSource)
        withContext(dispatchers.io) {
            currentsSource = getCurrentWeatherUseCase(latitude, longitude, forceRefresh)
        }
        _current.addSource(currentsSource) {
            if (it.data != null) {
                _current.value = it
                _dt.value = convertIntToTime(it.data?.dt)
            }
            if (it.status == Resource.Status.ERROR) _messageError.value =
                Event(R.string.an_error_happened)
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertIntToTime(time: Int?): String {
        var newTime: Long = time?.toLong()!!
        newTime = newTime.times(1000)
        val date = Date(newTime)
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return format.format(date)
    }
}