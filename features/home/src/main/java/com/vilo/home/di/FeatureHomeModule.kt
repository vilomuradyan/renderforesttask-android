package com.vilo.home.di

import com.vilo.home.domain.GetCurrentWeatherUseCase
import com.vilo.home.views.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val featureHomeModule = module {
    factory { GetCurrentWeatherUseCase(get()) }
    viewModel { HomeViewModel(get(),get(), get(named("settingsPrefs"))) }
}