package com.vilo.home.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.vilo.common.utils.Constants
import com.vilo.model.Current
import com.vilo.repository.WeatherRepository
import com.vilo.repository.utils.Resource

class GetCurrentWeatherUseCase(private val repository: WeatherRepository) {

    suspend operator fun invoke(latitude: Double, longitude: Double, forceRefresh: Boolean): LiveData<Resource<Current>> {
        return Transformations.map(repository.getCurrentWithCache(latitude, longitude, forceRefresh)) {
            it.data?.let {current->
                current.temp = current.temp.minus(273.15)
                current.weather.let { weather->
                    weather.first().icon = Constants.IMAGE_URL + weather.first().icon + Constants.IMAGE_END_POINT
                }
            }
            it
        }
    }
}