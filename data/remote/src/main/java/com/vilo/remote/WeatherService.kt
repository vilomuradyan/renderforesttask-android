package com.vilo.remote

import com.vilo.model.BaseResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("/data/2.5/onecall")
    fun fetchCurrentAsync(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String = "minutely,hourly,daily",
        @Query("appid") key: String = "8207f3684e1a2728a457a50f9f740824"
    ): Deferred<BaseResponse>

    @GET("/data/2.5/onecall")
    fun fetchDailyAsync(
        @Query("lat") lat: Double = 40.21408008341849,
        @Query("lon") lon: Double = 44.544102319429165,
        @Query("exclude") exclude: String = "minutely,hourly,current",
        @Query("appid") key: String = "8207f3684e1a2728a457a50f9f740824"
    ): Deferred<BaseResponse>
}