package com.vilo.remote

class WeatherDataSource(private val weatherService: WeatherService) {

    fun fetchCurrentAsync(latitude : Double,
                          longitude : Double)
    = weatherService.fetchCurrentAsync(latitude, longitude)
    fun fetchDailyAsync() = weatherService.fetchDailyAsync()
}