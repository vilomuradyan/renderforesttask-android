package com.vilo.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vilo.model.FeelsLike

object FeelsLikeConverters {
    @JvmStatic
    @TypeConverter
    fun stringToMeasurements(json: String?): FeelsLike {
        val gson = Gson()
        val type =
            object : TypeToken<FeelsLike>() {}.type
        return gson.fromJson(json, type)
    }

    @JvmStatic
    @TypeConverter
    fun measurementsToString(feelsLike: FeelsLike): String {
        val gson = Gson()
        val type =
            object : TypeToken<FeelsLike>() {}.type
        return gson.toJson(feelsLike, type)
    }
}