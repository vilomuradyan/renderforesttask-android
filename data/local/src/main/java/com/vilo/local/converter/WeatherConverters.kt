package com.vilo.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vilo.model.Weather

object WeatherConverters {
    @JvmStatic
    @TypeConverter
    fun stringToMeasurements(json: String?): List<Weather> {
        val gson = Gson()
        val type =
            object : TypeToken<List<Weather?>?>() {}.type
        return gson.fromJson(json, type)
    }

    @JvmStatic
    @TypeConverter
    fun measurementsToString(list: List<Weather?>?): String {
        val gson = Gson()
        val type =
            object : TypeToken<List<Weather?>?>() {}.type
        return gson.toJson(list, type)
    }
}