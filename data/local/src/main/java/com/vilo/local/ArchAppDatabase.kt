package com.vilo.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vilo.local.converter.FeelsLikeConverters
import com.vilo.local.converter.TempConverters
import com.vilo.local.converter.WeatherConverters
import com.vilo.local.dao.CurrentDao
import com.vilo.local.dao.DailyDao
import com.vilo.model.Current
import com.vilo.model.Daily

@Database(entities = [Current::class, Daily::class], version = 2, exportSchema = false)
@TypeConverters(WeatherConverters::class, FeelsLikeConverters::class, TempConverters::class)
abstract class ArchAppDatabase: RoomDatabase() {

    abstract fun CurrentDao() : CurrentDao
    abstract fun DailyDao() : DailyDao

    companion object {

        fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, ArchAppDatabase::class.java, "ArchApp.db")
                .fallbackToDestructiveMigration().build()
    }
}