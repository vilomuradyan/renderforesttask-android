package com.vilo.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import com.vilo.model.Current

@Dao
abstract class CurrentDao : BaseDao<Current>() {

    @Query("SELECT * FROM Current")
    abstract suspend fun getCurrent(): Current

    suspend fun save(current: Current) {
        insert(current)
    }

    @Query("DELETE FROM Current")
    abstract suspend fun deleteAll()

}