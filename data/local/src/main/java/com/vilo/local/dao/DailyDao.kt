package com.vilo.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.vilo.model.Daily

@Dao
abstract class DailyDao : BaseDao<Daily>() {

    @Query("SELECT * FROM Daily ")
    abstract suspend fun getDailyList(): List<Daily>

    @Query("SELECT * FROM Daily")
    abstract suspend fun getDaily(): Daily

    suspend fun save(daily: Daily) {
        insert(daily)
    }
    suspend fun save(dailyList : List<Daily>) {
        insert(dailyList)
    }
}