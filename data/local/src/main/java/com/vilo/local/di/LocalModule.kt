package com.vilo.local.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.vilo.local.ArchAppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val DATABASE = "DATABASE"

val localModule = module {
    single(named(DATABASE)) { ArchAppDatabase.buildDatabase(androidApplication()) }
    factory { (get(named(DATABASE)) as ArchAppDatabase).CurrentDao() }
    factory { (get(named(DATABASE)) as ArchAppDatabase).DailyDao() }
    single(named("settingsPrefs")) { provideSettingsPreferences(androidApplication()) }
    single(named("securePrefs")) { provideSecurePreferences(androidApplication()) }
}

private const val PREFERENCES_FILE_KEY = "com.vilo.settings_preferences"
private const val SECURE_PREFS_FILE_KEY = "com.vilo.secure_preferences"

private fun provideSettingsPreferences(app: Application): SharedPreferences =
    app.getSharedPreferences(PREFERENCES_FILE_KEY, Context.MODE_PRIVATE)

private fun provideSecurePreferences(app: Application): SharedPreferences =
    app.getSharedPreferences(SECURE_PREFS_FILE_KEY, Context.MODE_PRIVATE)