package com.vilo.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vilo.model.Temp

object TempConverters {
    @JvmStatic
    @TypeConverter
    fun stringToMeasurements(json: String?): Temp {
        val gson = Gson()
        val type =
            object : TypeToken<Temp>() {}.type
        return gson.fromJson(json, type)
    }

    @JvmStatic
    @TypeConverter
    fun measurementsToString(temp: Temp): String {
        val gson = Gson()
        val type =
            object : TypeToken<Temp>() {}.type
        return gson.toJson(temp, type)
    }
}