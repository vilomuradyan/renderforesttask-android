package com.vilo.repository.di

import com.vilo.repository.AppDispatchers
import com.vilo.repository.WeatherRepository
import com.vilo.repository.WeatherRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory {
        WeatherRepositoryImpl(
            get(),
            get(),
            get()
        ) as WeatherRepository
    }
}