package com.vilo.repository

import androidx.lifecycle.LiveData
import com.vilo.local.dao.CurrentDao
import com.vilo.local.dao.DailyDao
import com.vilo.model.BaseResponse
import com.vilo.model.Current
import com.vilo.model.Daily
import com.vilo.remote.WeatherDataSource
import com.vilo.repository.utils.NetworkBoundResource
import com.vilo.repository.utils.Resource
import kotlinx.coroutines.Deferred

interface WeatherRepository {
    suspend fun getCurrentWithCache(
        latitude: Double,
        longitude: Double, forceRefresh: Boolean
    ): LiveData<Resource<Current>>

    suspend fun getDailyWithCache(
        latitude: Double,
        longitude: Double, forceRefresh: Boolean
    ): LiveData<Resource<List<Daily>>>
}

class WeatherRepositoryImpl(
    private val dataSource: WeatherDataSource,
    private val currentDao: CurrentDao,
    private val dailyDao: DailyDao
) : WeatherRepository {

    override suspend fun getCurrentWithCache(
        latitude: Double,
        longitude: Double,
        forceRefresh: Boolean
    ): LiveData<Resource<Current>> {
        return object : NetworkBoundResource<Current, BaseResponse>() {

            override fun processResponse(response: BaseResponse): Current = response.current

            override suspend fun saveCallResults(items: Current) {
                currentDao.deleteAll()
                currentDao.save(items)
            }
            override fun shouldFetch(data: Current?): Boolean = data == null || forceRefresh

            override suspend fun loadFromDb(): Current = currentDao.getCurrent()

            override fun createCallAsync(): Deferred<BaseResponse> =
                dataSource.fetchCurrentAsync(latitude, longitude)

        }.build().asLiveData()
    }

    override suspend fun getDailyWithCache(
        latitude: Double,
        longitude: Double, forceRefresh: Boolean
    ): LiveData<Resource<List<Daily>>> {
        return object : NetworkBoundResource<List<Daily>, BaseResponse>() {

            override fun processResponse(response: BaseResponse): List<Daily> = response.daily

            override suspend fun saveCallResults(items: List<Daily>) = dailyDao.save(items)

            override fun shouldFetch(data: List<Daily>?): Boolean = data == null || data.isEmpty() || forceRefresh

            override suspend fun loadFromDb(): List<Daily> = dailyDao.getDailyList()

            override fun createCallAsync(): Deferred<BaseResponse> = dataSource.fetchDailyAsync()

        }.build().asLiveData()
    }
}