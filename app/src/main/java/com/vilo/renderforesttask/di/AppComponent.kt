package com.vilo.renderforesttask.di

import com.vilo.common.utils.Constants
import com.vilo.detail.di.featureDetailModule
import com.vilo.home.di.featureHomeModule
import com.vilo.local.di.localModule
import com.vilo.remote.di.createRemoteModule
import com.vilo.repository.di.repositoryModule

val appComponent= listOf(
    createRemoteModule(Constants.BASE_URL),
    repositoryModule,
    featureHomeModule, featureDetailModule,
    localModule
)