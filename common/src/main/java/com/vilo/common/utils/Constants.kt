package com.vilo.common.utils

object Constants {

    const val BASE_URL = "https://api.openweathermap.org/"
    const val IMAGE_URL = "http://openweathermap.org/img/wn/"
    const val IMAGE_END_POINT = "@4x.png"
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
}