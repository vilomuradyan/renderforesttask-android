package com.vilo.common.utils

import android.content.SharedPreferences

object Preference {

    fun saveLatitude(value : Float, sharedPreferences: SharedPreferences){
        val editor = sharedPreferences.edit()
        editor.putFloat(Constants.LATITUDE, value)
        editor.apply()
    }

    fun saveLongitude(value : Float, sharedPreferences: SharedPreferences){
        val editor = sharedPreferences.edit()
        editor.putFloat(Constants.LONGITUDE, value)
        editor.apply()
    }

    fun getLatitude(sharedPreferences: SharedPreferences) = sharedPreferences.getFloat(Constants.LATITUDE, 0f)
    fun getLongitude(sharedPreferences: SharedPreferences) = sharedPreferences.getFloat(Constants.LONGITUDE, 0f)
}