package com.vilo.common.extension

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.vilo.common.utils.Event


fun Fragment.showMessage(message: String, timeLength: Int) {
    activity?.let {
         Toast.makeText(it, message, timeLength).show()
    }
}

fun Fragment.setupMessage(lifecycleOwner: LifecycleOwner, snackbarEvent: LiveData<Event<Int>>, timeLength: Int) {
    snackbarEvent.observe(lifecycleOwner, { event ->
        event.getContentIfNotHandled()?.let { res ->
            context?.let { showMessage(it.getString(res), timeLength) }
        }
    })
}