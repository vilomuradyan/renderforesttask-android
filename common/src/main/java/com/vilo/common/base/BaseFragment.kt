package com.vilo.common.base

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.vilo.common.extension.setupMessage
import com.vilo.navigation.NavigationCommand

abstract class BaseFragment: Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigation(getViewModel())
        setupMessage(this, getViewModel().messageError, Snackbar.LENGTH_LONG)
    }

    abstract fun getViewModel(): BaseViewModel

    private fun observeNavigation(viewModel: BaseViewModel) {
        viewModel.navigation.observe(viewLifecycleOwner, {
            it?.getContentIfNotHandled()?.let { command ->
                try {
                    when (command) {
                        is NavigationCommand.To -> findNavController().navigate(command.directions, getExtras())
                        is NavigationCommand.Back -> findNavController().navigateUp()
                    }
                }catch (e: Exception){
                    Log.e("navigation", "An error happened: $e")
                }

            }
        })
    }
    open fun getExtras(): FragmentNavigator.Extras = FragmentNavigatorExtras()
}